---
hide:
  - navigation
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe 1 Byteshield

På dette website finder du projektplaner, læringslogs og andet relateret til forår semesterprojektet på IT-Sikkerhed

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side) 

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)

<section class="image-section">
    <div>
      <div>
        <header>
          <h1>Gruppemedlemmerne</h1>
        </header>
        <div class="image-container">
          <figure>
            <img src="images/ProfilePhoto/Christian.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h2>Christian</h2>
              <h3>2.Semester - IT-teknolog</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/christian-m-455b31134/">
                  <img src="images/social/logo/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a>
                  <img src="images/social/logo/github.png" class="logo-image-disabled"></a>
                <!-- Email -->
                <a href="mailto:cnkm37558@edu.ucl.dk">
                  <img src="images/social/logo/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/Rivvi">
                  <img src="images/social/logo/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </figure>
          <figure>
            <img src="images/ProfilePhoto/Jesper.png" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h2>Jesper</h2>
              <h3>1.Semester - Datamatiker</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/jesper-becker-8a3b0b156/">
                  <img src="images/social/logo/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/jpbe99">
                  <img src="images/social/logo/github.png" class="logo-image"></a>
                <!-- Email -->
                <a href="mailto:jpbe38135@edu.ucl.dk">
                  <img src="images/social/logo/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/jesper.p.becker">
                  <img src="images/social/logo/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </figure>
          <figure>
            <img src="images/ProfilePhoto/John2.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h2>John</h2>
              <h3>1.Semester - Datamatiker</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/john-lange-4597901b9/">
                  <img src="images/social/logo/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/Sneizzle">
                  <img src="images/social/logo/github.png" class="logo-image"></a>
                <!-- Email -->
                <a href="mailto:jkla40320@edu.ucl.dk">
                  <img src="images/social/logo/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/draygoh">
                  <img src="images/social/logo/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  </section>
  