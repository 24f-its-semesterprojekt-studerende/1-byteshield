---
 hide:
#   - footer
---

# Dokumentation af hvordan man opsætter ubuntu server med Apache og Nginx

## Instruktioner

### Apache og Nginx Server opsætning

OBS: Denne guide dækker ikke opsætningen af Ubuntu Server, for en detaljeret gennemgang se [Ubuntu opsætning](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/documentation/ss_documentation_Ubuntu/#ubuntu-server-opstning), derudover følger Nginx opsætningen mange af de samme trin some apache setup.

![image](images/ApacheNginxsetup/1.png)

- På Target serveren bliver kommandoen "sudo apt install apache2" kørt for at installere apache

![image](images/ApacheNginxsetup/2.png)

- CD ind i mappen sites-available

![image](images/ApacheNginxsetup/3.png)

- herefter kopieres default config filen til en ny config fil

![image](images/ApacheNginxsetup/4.png)

- Her ses layoutet

![image](images/ApacheNginxsetup/5.png)

- Kommandoen "sudo a2ensite gci.conf" køres herefter for at enable siden med det nye config, herefter køres kommandoen "systemctl reload apache2" for at genindlæse configuration filen.

![image](images/ApacheNginxsetup/6.png)

- Herefter vælges der en bruger til at authenticate.
