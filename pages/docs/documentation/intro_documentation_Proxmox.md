---
 hide:
#   - footer
---

# Dokumentation af hvordan vi har opgraderet en server op opsat Proxmox på den given server

## Instruktioner

### Opsætning af proxmox server

!!! Info "sørg for at have sat usb som boot medie"
 
- Det første du ser at er som du på billedet forneden, herefter kommer proxmox installer GUI frem.

![Image 1](images/ProxmoxOpsætning/SImage_1.jpg)
![Image 2](images/ProxmoxOpsætning/SImage_2.jpg)

- På billedet foroven kan du vælge hvilken installer du benytte.
- Vi valgte Graphical.
- Scroll ned af denne side.

![Image 3](images/ProxmoxOpsætning/SImage_3.jpg)
![Image 4](images/ProxmoxOpsætning/SImage_4.jpg)
![Image 5](images/ProxmoxOpsætning/SImage_5.jpg)

- Her skal du vælge dit land, tidzone og keyboad layout.
- Vi valgte Danmark, dansk tid, dansk keyboard layout.

![Image 6](images/ProxmoxOpsætning/SImage_6.jpg)

- Her skal du opsætte en email og et password. 
- Denne email vil få notifikationer om ens proxmox server.

![Image 7](images/ProxmoxOpsætning/SImage_7.jpg)

- Dette er de forskellige gruppers ip-adresser som man skal benytte i det næste step.
!!! Note "Vi er gruppe 1 så vi brugte naturligvis den øverste"

![Image 8](images/ProxmoxOpsætning/SImage_8.jpg)

- Nu skal du opsætte:
    - Hostname
    - IP-adresse
    - Gateway
    - DNS server

- Se billedet forneden med de korrekte indstillinger
!!! Warning "Hostname skal være uden bundstreg  = _"
!!! Note "Vi endte med at skulle geninstallere Proxmox begrund af vi havde brugt bundstreg i vores Hostname som set på billedet forneden"

![Image 9](images/ProxmoxOpsætning/SImage_9.jpg)

- Billedet foroven viser hvordan netværket indstillingerne skal opsættes.

![Image 10](images/ProxmoxOpsætning/SImage_10.jpg)

- Tilsidst får du fremvist en opsamlings side hvor du selvfølgelig gennemgår om alt er som det skal være
- Hvis alt er korrekt efter din mening kan du gå videre, og du vil se noget lignende som billedet forneden.

![Image 11](images/ProxmoxOpsætning/SImage_11.jpg)
![Image 12](images/ProxmoxOpsætning/SImage_12.jpg)

- Når du ser denne side så er det installeret.
!!! info "kan godt være du skal geninstallere det hvis det ikke virker!"

![Image 13](images/ProxmoxOpsætning/SImage_13.jpg)

!!! Success "Hvis du kan gå ind på web interface"
!!! Failure "Hvis du ikke kan få forbindelse til web interface"
!!! Info "Hvis der ikke er hul igennem til web interface skal du prøve at geninstallere"
