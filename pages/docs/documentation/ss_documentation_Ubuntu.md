---
 hide:
#   - footer
---

# Dokumentation af hvordan man opsætter ubuntu server på virtualbox

## Instruktioner

### Ubuntu Server opsætning
- Første step:
    Som man kan se på billedet for neden er der de 5 knapper:
    - præfrencer
    - Import
    - Export
    - Ny
    - Tilføj
- Tryk på "Ny"

![Image 1](images/OPNsenseProxmoxSetup/Image_1.png)

- Nu skal du så vælge navn på maskinen, jeg har valgt navnet "UbuntuServer"
- Du skal vælge ISO filen du vil bruge, i dette tilfælde bruger Ubuntu Server iso fil 

!!! Info "Sørg for at have downloadet Ubuntu Server Iso filen fra deres <a href="https://ubuntu.com/download/server" target="_blank">hjemmeside</a>"

![Image 2](images/UbuntuGuide/Image_2.png)

- Klik på tab "Unattended Install" som vist på billedet forneden
- Indtast et Username og Password
- Sørg for at Hostname er "UbuntuServer" uden qoutes.

![Image 3](images/UbuntuGuide/Image_3.png)

- I hardware tab kan du vælge hvor mange processor or hvor meget ram du vil tildele maskinen, jeg efterladt det på default
- 1 processor og 2048mb ram
- I Harddisk Tab tildele du hvor meget Harddisk plads du vil tildele maskinen

![Image 4](images/UbuntuGuide/Image_4.png)

- Tryk Finish og maskinen vil starte op med det samme og du vil se et billede ligende med det forneden
- Blot vent et lille sekunds tid og den vil vise næste menu

![Image 5](images/UbuntuGuide/Image_5.png)

- Her vælger du dit fortrækkende sprog du vil have maskinen til være på.
- jeg vælger Engelsk da, det jeg forstår ud af alle valgene.
- Du kan dog vælge at åbne dine horisonter og vælge português og på den måde både lære Ubuntu og portugisisk på en gang, Det skal dog nævnes at der er forhøjet risiko for at du river hårene ud af frustration senere hen xD 

![Image 6](images/UbuntuGuide/Image_6.png)

- Vælg dit keyboard layout manuelt, Den automatisk keyboad identifer er knap så god efter min mening.


![Image 7](images/UbuntuGuide/Image_7.png)

- Vælg Ubuntu Server
- Tryk Done

![Image 8](images/UbuntuGuide/Image_8.png)

- Her skal du op og ændre i "enp0s3"
- Vælg "Edit IPv4"

![Image 9](images/UbuntuGuide/Image_9.png)

- Nu indtast samme ínformation som vist på billedet forneden og tryk save
- Når du er tilbage til "network connections" hvor redigeret "enp0s3" tryk på Done

![Image 10](images/UbuntuGuide/Image_10.png)

- Her skal du blot indtaste et navn, servernavn, username og password
- Jeg har valgt Ubu, du kan kalde dit lige hvad du har lyst til.

![Image 11](images/UbuntuGuide/Image_11.png)
![Image 12](images/UbuntuGuide/Image_12.png)

- Nu installere Ubuntu vent til du ser et noget ligende som på billedet forneden
- Tryk Reboot Now
- Når du har rebooted, login med dine credientials og kør kommandoen "sudo apt update && sudo apt upgrade"

![Image 13](images/UbuntuGuide/Image_13.png)

!!! Warning "De næste step kræver at du har installeret <a href="https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_3_Ops%C3%A6tning_Kali_Linux/" target="_blank">Kali Linux</a>"

??? Info "Udfold Mig for skriftlig guide til billedet forneden"
    === "Dansk"
        
        - Step 1: klik på det lille icon med de 3 linjer på tools.
        - Step 2: klik på Netværk.
        - Step 3: Klik på NAT Netværk.
        - Step 4: Klik på Skab knappen
        - Step 5: Klik på Enable DHCP, hvis den allerede har et flueben, ellers se bort fra dette step.
        - Step 6: Klik på Ok.
        - Step 7: Højreklik på din Kali Linux Maskine.
        - Step 8: Klik på indstillinger.
        - Step 9: Klik på netværk.
        - Step 10: Klik på den øverste menu og vælg NAT netværk
        - Step 11: Klik på den nederste menu og vælg det NAT netværk du specificeret i step 4 og 5.
        - Step 12: Klik og udvid Avanceret menuen
        - Step 13: Til højre for MAC adresse er der en knap tryk på den.
    
    === "English"
        
        - Step 1: Click on the small icon with the three lines on Tools.
        - Step 2: Click on Network.
        - Step 3: Click on NAT Network.
        - Step 4: Click on the Create button.
        - Step 5: Click on Enable DHCP, if it's already checked, otherwise skip this step.
        - Step 6: Click on OK.
        - Step 7: Right-click on your Kali Linux Machine.
        - Step 8: Click on Settings.
        - Step 9: Click on Network.
        - Step 10: Click on the top menu and select NAT Network.
        - Step 11: Click on the bottom menu and select the NAT Network you specified in step 4 and 5.
        - Step 12: Click to expand the Advanced menu.
        - Step 13: To the right of MAC address, there is a button, press it.
        

![Animation](images/UbuntuGuide/Animation_xx.gif)

- I kali åbn en terminal og kør kommandoen som set på billedet forneden

![Image 15](images/UbuntuGuide/Image_15.PNG)

- På din Ubuntu server kør kommandoen "ping [Din Kali IP-Adresse]" Som set på billedet forneden med min ip
- Du skulle nu gerne se noget ligende på din Kali og Ubuntu terminal som på billedet forneden

![Image 17](images/UbuntuGuide/Animation_17.gif)

- Tilsidst tryk "Ctrl C" i din ubuntu maskine for at stoppe med at ping.

!!! beer "Mission accomplished - Go drink a well deserved beer!"

