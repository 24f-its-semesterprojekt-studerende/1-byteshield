---
 hide:
#   - footer
---

# Dokumentation af Kali installation på Proxmoxserver

## Information

- Dette er instruktioner til hvordan man installere Kali på en proxmox server

## Instruktioner

![image 1](images/OPNsenseKaliSetup/1.png)

- Før installationen af Kali kan påbegynde skal man først have fat i en ISO af distroen, dette kan gøres på to forskellige måder: 1. Kopir URL'et til downloaden og indsæt det under "Download from URL" funktionen på Proxmox; 2. Upload ISO filen fra et lokalt drev. Processen efter er den samme. Denne guide tager udgangspunkt i at filen bliver downloadet via URL.

![image 2](images/OPNsenseKaliSetup/2.png)

- Foroven ses det at ISO filen bliver downloadet, når downloaden er færdig vil man se outputtet "TASK OK"  

![image 3](images/OPNsenseKaliSetup/3.png)

- En ny virtual machine (VM) skal nu oprettes hvorpå man kan installere ISO'en, som det ses på billedet kan dette gøres ved at højre-klikke på Noden, herefter vælges "Create VM".

![image 4](images/OPNsenseKaliSetup/4.png)

- Denne VM skal herefter opsættes efter følgende konfiguration, vigtist her er at konfigurere CPU, hukommelse og netværk. Efter dette er gjort, klik finish og følg herefter installationen.
*OBS: Under installationen er det vigtigt at vælge en pakke med en GUI!* 

![image 5](images/OPNsenseKaliSetup/5.png)

- Når installationen er fuldført kan man nu bruge Kali som normalt, i dette tilfælde skal det testes om maskinen kan tilgå OPNsense routeren.

![image 6](images/OPNsenseKaliSetup/6.png)

- Som set foroven er det muligt at tilgå routeren via IP'en 192.168.1.1, som er gateway for netværket.


## Links