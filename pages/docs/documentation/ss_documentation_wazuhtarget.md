---
 hide:
#   - footer
---

# Dokumentation af hvordan man opsætter ubuntu server på virtualbox

## Instruktioner

### Wazuh og target Server opsætning

OBS: Dette er en hurtig gennemgang af server opsætningen, for en mere detaljeret gennemgang se [Ubuntu opsætning](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/documentation/ss_documentation_Ubuntu/#ubuntu-server-opstning)

![image](images/wazuhtarget/p1.png)
![image](images/wazuhtarget/p2.png)
![image](images/wazuhtarget/p3.png)
![image](images/wazuhtarget/p4.png)
![image](images/wazuhtarget/p5.png)
Bemærk her at tre brugere bliver oprettet og lavet til super users med kommandoerne "useradd" og "usermod" med flagene a (append) G(groups), hvilket betyder at vi tilføjer brugeren til gruppen super user.