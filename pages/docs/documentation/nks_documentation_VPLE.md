---
 hide:
#   - footer
---

# Dokumentation af hvordan man opsætter VPLE

## Instruktioner

### VPLE opsætning

![image](images/VPLEsetup/1.png)

- Åben shell ved at højre klikke noden.

![image](images/VPLEsetup/2.png)

- Kommandoen "tar -xvf VPLE.ova" køres for at konvertere OVA filen til OVF.

![image](images/VPLEsetup/3.png)

- Som det ses her skal det gøres i tmp folderen, da det er her OVA filen blev downloadet til.

![image](images/VPLEsetup/4.png)

- Herefter kan kommanden "qm importovf 200 /tmp/VPLE.ovf local lvm" køres for at importere VM'en så den er brugbar.

![image](images/VPLEsetup/5.png)

- Her kan det ses at maskinen nu vises blandt de andre maskinen i noden.

![image](images/VPLEsetup/6.png)

- Herefter kan et network interface tilføjes til maskinen så den kan tilgås af de andre maskiner.