---
 hide:
#   - footer
---

# Dokumentation af x

## Information

- Dette er vores dokumentation for hvordan man installerer wazuh på ubuntu server.
- Dette gøres på vores Ubuntu server vi har på Proxmox.

## Instruktioner

- Start din Ubuntu server.
- skriv dette ```curl -sO https://packages.wazuh.com/4.7/wazuh-install.sh && sudo bash ./wazuh-install.sh -a```
- tryk Enter
- vent til du ser.
![image1](images/wazuhInstallation/InstallFinsished.png)

- Dette er et billede af vores installation.
![Installing](images/wazuhInstallation/Installing2.png)

- Her efter tjek om du kan få forbindelse til wazuh webinterface ved at bruge din kali maskine på Proxmox.
- login med dine credentials
- brugernavn: admin
- password: meget langt se bare billedet foroven.
![Webinterface](images/wazuhInstallation/WebInterface.png)


- Et billede af de processer wazuh bruger og hvilken konto der kører dem.
![Wazuhprocesses](images/wazuhInstallation/wazuhprocesses.png)

## Links

- [Wazuh opsætning](https://documentation.wazuh.com/current/quickstart.html)
- [Opgavebeskrivelsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/25_1_Setting_up_Wazuh/#links)