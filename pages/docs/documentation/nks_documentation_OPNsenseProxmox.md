---
 hide:
#   - footer
---

# Dokumentation af OPNsense installation på Proxmoxserver

## Information

- Dette er instruktioner til hvordan man installere OPNsense på en proxmox server

## Instruktioner

- Login ind på dit proxmox webinterface

![Image 1](images/OPNsenseProxmoxSetup/1.png)

- Lav en ny bridge til linux, som vist på billedet forneden

![Image 2](images/OPNsenseProxmoxSetup/2%20linux%20bridge.png)

- tryk create

![Image 3](images/OPNsenseProxmoxSetup/3.png)


- Tryk på "apply configuration" for aktivere den nyskabte bridge, som vist på billedet forneden.


![Image 9](images/OPNsenseProxmoxSetup/9%20mangler%20apply%20config.png)

- Upload OPNsense til proxmox

![Image 4](images/OPNsenseProxmoxSetup/4%20uploader%20iso%20af%20opnsense.png)

![Image 5](images/OPNsenseProxmoxSetup/5%20sucess%20uploaded.png)

![Image 6](images/OPNsenseProxmoxSetup/6%20konfigurerering%20af%20VM%20med%20isofil.png)

- Tilføj det nye netværkskort til den virtuelle maskine

![Image 7](images/OPNsenseProxmoxSetup/7%20%20tilføj%20netværkskort.png)

![Image 8](images/OPNsenseProxmoxSetup/8%20konfigurerering%20af%20nye%20netværkskort.png)

- Start den virtuelle maskine og tryk på konsollen, vent på at OPNsense starter.
- Log ind som: "installer" password: "opnsense" for at starte installationen.

![Image 10](images/OPNsenseProxmoxSetup/10%20login%20til%20opnsense%20proxmox%20halløj.png)

- Følg OPNsense Installationsguide 
- Eller kig på billederne forneden

![Image 11](images/OPNsenseProxmoxSetup/11%20opnsense%20installer.png)

![Image 12](images/OPNsenseProxmoxSetup/12%20installation.png)

![Image 13](images/OPNsenseProxmoxSetup/13%20installation%20af%20stripe%20ting.png)

![Image 14](images/OPNsenseProxmoxSetup/14%20installation%20zfs%20config.png)

![Image 15](images/OPNsenseProxmoxSetup/15%20confirm.png)

![Image 16](images/OPNsenseProxmoxSetup/15%20installation%20process.png)

- Reboot maskinen
- og sluk den derefter

![Image 17](images/OPNsenseProxmoxSetup/15%20finish%20and%20reboot.png)

- Dobbeltklik på CD/DVD drive og fjern image filen fra drevet.

![Image 18](images/OPNsenseProxmoxSetup/15%20disable%20media.png)

- start opnsense vm, der bootes nu fra harddisken istedet for dvd drevet
- Login med: "root" og password: "opnsense"

![Image 19](images/OPNsenseProxmoxSetup/16%20boot%20fra%20harddisk.png)

- Vælg assign interfaces, nej til LAGGs og VLANs

![Image 20](images/OPNsenseProxmoxSetup/17%20assign%20interface%20nej%20til%20lan%20og%20vlags.png)

- Assign WAN til vtnet0 og LAN til vtnet1, bekræft med y og vent på prompt.

![Image 21](images/OPNsenseProxmoxSetup/18%20assigning%20wan%20og%20lan%20til%20vnet0%20og%20vnet1.png)

![Image 22](images/OPNsenseProxmoxSetup/19%20assigning.png)

- Bekræft at WAN har en adresse på 10.56.18 netværket og LAN har 192.168.1.1.
Hvis man vil ændre LAN adressen kan man gøre det ved at vælge 2 Set interface IP address. Her kan man også vælge om DHCP skal være aktiveret på LAN netværket.
- Man bør nu kunne tilgå opnsense webinterfacet fra en browser på en proxmox vm tilsluttet vmbr10 på LAN adressen (192.168.1.1).


![Image 23](images/OPNsenseProxmoxSetup/20%20done.png)

## Links

- [Læringsmatriale](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/21_opnsense_proxmox/#links)
- Links til relevante online ressourcer