---
 hide:
#   - footer
---

# Dokumentation af opsætning af server hardware

## Instruktioner

### Opsætning af server hardware
![image 1](images/ServerHardwareOpsætning/1.jpg)

- Start med at kontrollere, om dit system fungerer korrekt, før du begynder at opgradere hardwaren.

- Tænd for systemet for at se, om det starter op normalt.
Observer, om systemet gennemfører POST (Power-On Self Test). Dette er en startsekvens, hvor systemet tester sine hardwarekomponenter for at sikre, at de fungerer korrekt.

- Hvis systemet ikke gennemfører POST og viser tegn på funktionsfejl, kan det være nødvendigt at finde en erstatning, der fungerer korrekt.

- Hvis du observerer, at systemet gennemfører POST og starter normalt op, kan du fortsætte med at opgradere hardwaren som planlagt.

![image 2](images/ServerHardwareOpsætning/2.jpg)

- Kontroller først, at der ikke er strøm i systemet, inden du åbner kabinettet. Dette er vigtigt, da der er indbyggede alarmer i kabinettet.

- På kabinettet skal du finde tappen, der bruges til at åbne det.
Tryk tappen ned.

- Træk panelet opad for at åbne det.

![image 3](images/ServerHardwareOpsætning/3.jpg)

- Foroven ses systemet i dets fulde, i denne guide arbejdes der primært med RAM og lagringsplads, RAM-pladserne kan ikke ses på billedet eftersom de er gemt under afskærmningen til blæseren, næste trin viser hvordan denne fjernes.

![image 4](images/ServerHardwareOpsætning/4.jpg)

- Foroven ses det, at afskærmningen fjernes ved at trykke de to tapper ind, hvorefter man forsigtigt kan løfte den af.

![image 5](images/ServerHardwareOpsætning/5.jpg)

- Fjern afskærmningen for at få adgang til RAM-pladserne.
Identificer, hvor de gamle RAM-moduler var placeret, eller konsulter manualen for at bestemme de rigtige pladser til de nye RAM-moduler.

- I dette tilfælde kan de samme pladser bruges, baseret på information fra manualen. Bemærk, at RAM-pladserne på den anden side af varmespredningen er spejlvendt.

- For at fjerne et RAM-modul, skal du trykke ned på de hvide eller sorte låsetapper.

- Når låsetapperne er trykket ned, kan RAM-modulet sikkert fjernes fra pladsen.

![image 6](images/ServerHardwareOpsætning/6.jpg)
![image 7](images/ServerHardwareOpsætning/7.jpg)

- På billederne ses de nye RAM-moduler, før de installeres i systemet.

- Systemet bliver opgraderet fra 16GB RAM til 64GB.

![image 8](images/ServerHardwareOpsætning/8.jpg)

- Orientér de nye RAM-moduler korrekt ved at observere udhakene mellem "benene" på selve modulet.
Kontroller RAM-pladsen og find det tilsvarende stykke plastik, der forhindrer forkert orientering af modulet.

- Indsæt forsigtigt modulet i RAM-pladsen, sørg for at matche udhakene med plastikstykket på pladsen.

- Tryk forsigtigt ned på modulet, indtil du hører et klik. Dette betyder, at låsetapperne er låst.

- Gentag processen for at installere alle nye RAM-moduler.

- Når alle modulerne er korrekt installeret og låst på plads, kan du sætte afskærmningen tilbage på pladsen.

![image 9](images/ServerHardwareOpsætning/9.jpg)

- Næste trin er at installere ny lagring, i alt skal systemet have 960GB, hvorvidt disse køre i RAID eller separat er op til den enkelte gruppe.

![image 10](images/ServerHardwareOpsætning/10.jpg)

- Før installationen af den nye lagring skal frontpanelet låses op, dette giver adgang til harddisk-pladserne.

![image 11](images/ServerHardwareOpsætning/11.jpg)

- Efter frontpanelet er fjernet, ses harddisk-pladserne nederst på kabinettet, tryk ind på fordybningen og træk forsigtigt ud.

![image 12](images/ServerHardwareOpsætning/12.jpg)

- For at fjerne den gamle SSD, skal du forsigtigt bøje plastikvæggene væk fra SSD'en med tilstrækkelig kraft, indtil den løsnes.

![image 13](images/ServerHardwareOpsætning/13.jpg)

- For at installere de nye SSD'er skal du gentage processen ved at bøje væggene væk, indtil SSD'en klikker på plads. Når dette er gjort, kan de nye SSD'er sættes tilbage i kabinettet. Efterfølgende skal systemet samles igen, og man kan derefter kontrollere i BIOS, om alt er installeret korrekt.

![image 14](images/ServerHardwareOpsætning/14.jpg)
![image 15](images/ServerHardwareOpsætning/15.jpg)

- På billederne foroven ses det at systemet genkender både RAM og lagring, og man kan derfor gå videre til installation af operativsystemet.

![image 16](images/ServerHardwareOpsætning/16.jpg)

- Sluk systemet og indsæt boot mediet, i dette tilfælde en USB. Se herefter Proxmox opsætningsguide.

## Links
- [Manual til Dell Precision 5820](https://www.dell.com/support/manuals/da-dk/precision-5820-workstation/precision_5820_om_pub/notes-cautions-and-warnings?guid=guid-5b8de7b7-879f-45a4-88e0-732155904029&lang=en-us)

- [Proxmox opsætningsguide](/1-byteshield/documentation/intro_documentation_Proxmox/)