---
 hide:
#  - footer
---

# Læringslog uge 11 - *Systemsikkerhed | Logning*

## Emner
- Logning

## Mål for ugen


### Praktiske mål

- Den studerende kan redegøre for hvornår der som minimum bør laves en log linje
- Den studerende har grundlæggende forståelse for log management

**Praktiske opgaver vi har udført**
## Vi har i løbet af dagen arbejdet med 2 opgaver:

### [1. Opgave 12 | Linux log system.](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/12_Linux_system_og_auth_logs/#links)
??? Info "Åbn mig for at se opgave dokumentation"
    ![image](images/Week_11/syslog%20udskriv.PNG)
    ![image](images/Week_11/syslog%20udsnit.PNG)
    - Formatet er dato, bruger og besked.
    - Tidszonen er UTC
    ![image](images/Week_11/Authlog%20udskrivUdsnit.PNG)
    ![image](images/Week_11/Authlog%20udskrivUdsnitRoot.PNG)
    ![image](images/Week_11/Authlog%20udskrivUdsnitNormalUseragain.PNG)
    - Vi kan se at vi skifter fra normal user til Root, og tilbage igen til normal user.
    

### [2. Opgave 25.2 | Eftermiddag - Opsætning af Wazuh agent.](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/25_2_Setting_up_Wazuh_Agent/)
??? Info "Åbn mig for at se opgave dokumentation"
    !!! Warning "Nogle af billederne er svære at se, klik på dem for at forstørre dem!"
    - Vi fulgte denne [guide](https://documentation.wazuh.com/current/installation-guide/wazuh-agent/wazuh-agent-package-linux.html)
        - Vi brugte APT versionen.
        - Vi fulgte hele guiden indtil "disable wazuh updates"
    [![Image](images/Week_11/Wazuh1.png)](images/Week_11/Wazuh1.png)
    [![Image](images/Week_11/WazuhTarget.png)](images/Week_11/WazuhTarget.png)
    - Efter at opsat wazuh agent kan vi tjekke om vi har gjort det korrekt ved at se om den vises i vores webportal som set på billederne foroven.
    - Alternativt kan man også på sin wazuh ubuntu server maskine skrive kommandoen som på billdet forneden for at se om det er korrekt installeret.
    [![Image](images/Week_11/CheckWazuhStatus.png)](images/Week_11/CheckWazuhStatus.png)
    - Her efter per opgavens instruktioner vil vi gerne "trigger" et security event.
    - så vi tilføjer en bruger på vores target maskine, der hedder "DARTH"
    [![Image](images/Week_11/AdduserDarth.png)](images/Week_11/AdduserDarth.png)
    - Vi kan så se på vores Wazuh webportal at der er kommet et nyt security event, at en ny bruger er blevet oprettet.
    - Der er endda en MITRE ID på eventet.
    [![Image](images/Week_11/SecurityEventDarth.png)](images/Week_11/SecurityEventDarth.png)
    - Her kan vi så se lidt flere detaljer omkring det event ifølge MITRE.
    [![Image](images/Week_11/MITRE_ID_DARTH.png)](images/Week_11/MITRE_ID_DARTH.png)
    
    
    
    
        

### Læringsmål

**Læringsmål vi har arbejdet med**

### Viden:
- Generelle governance principper
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
### Færdighed
- Implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
### Kompetencer:
- Håndtere udvælgelse af praktiske mekanismer til at detektere it-sikkerhedsmæssige hændelser.

## Reflektioner over hvad vi har lært

=== "Christian"
    - Ingen refleksioner her tjek en af de andre medlemmer😴
    
=== "Jesper"
    - Jeg synes generelt set at undervisning var lærerig.
    - I vores gruppe var der en del problemer med at for Wazuh agent til at fungere.
        1. Den ubuntu det skulle være på havde intet internet så vi kunne ikke installere vores key some man skulle i guiden.
        2. Kommandoerne man skulle bruge var så lange uden at man kunne kopiér dem ind, at det decideret var en værre form for tortur end at skulle assistere sin bedstemor med et IT problem over et videoopkald. 😭
            - I hvertfald hvis kommandoerne ikke blev eksekveret i første omgang.
        3. Vi havde lidt glem adgangskoden til wazuh webportalen..
    - Dog fik vi løst alle disse forhindringer. Er vi blevet klogere er så et helt andet spørgsmål, som jeg har brug mere refleksionstid til.
    - Den indviduelle opgave i Linux log system, var ret lige til uden problemer endda. Her lærte jeg at der er 2 forskellige logs som logger 2 forskellig ting.
        - F.eks. AUTHLOG som logger hændelse som f.eks hvis man skifter bruger.
    - Jeg synes wazuh er ret spænende så længere man har medvind. Når der er modvind er det knap så fedt, men det er vel egentlig når der er modvind man lærer noget, så det har også sit sted, bare ikke sidst på dagen hvor hovedet er tungt og man er træt. 
=== "John"
    - Ingen refleksioner her tjek en af de andre medlemmer😴
    
## Andet

