---
 hide:
#  - footer
---

# Læringslog uge 08 - *introduktion til CIS18 og Mitre ATT&CK*

## Emner

- Ugens emner er:
    - Vi er blevet introduceret til grundlæggende viden omkring CIS18 kontroller og benchmarks
    - Vi er også blevet introduceret til Mitre att&ck

## Mål for ugen

At danne sig et kendskab til CIS18 og Mitre.

### Praktiske mål

**Praktiske opgaver vi har udført**
## Vi har i løbet af dagen arbejdet med 3 opgaver:

### 1. Vurdering af en fiktiv virksomhed og dens IT-sikkerhedsmodenhedsniveau.
- I denne opgave skulle vi vurdere en virksomhed ud fra CIS18 kontrollerne og om de overholder disse.

### 2. General orientering om Mitre ATT&CK-rammeværket og hvad det kan bruges til.
- Her har vi udforsket Mitre og forskellige teknikker og angrebsflader beskrevet i rammeværket f.eks. "TA0004 privilege escalation".

- I forbindelse med dette har vi også lavet en lille præsentation til uge 9.

### 3. Diskussion om diverse taktikker indenfor førnævnte rammeværk

- Her har vi diskuteret hvordan og hvorledes forksellige taktikker kan bruges og hvad formålet med disse kunne være.

### Læringsmål

**Læringsmål vi har arbejdet med**

### Viden:
- CIS18 kontroller, benchmarks og Mitre rammeværket

### Kompetencer:
- Den studerende har dannet sig kendskab til CIS18 kontroller og benchmarks samt Mitre rammeværket

## Reflektioner over hvad vi har lært

=== "Christian"

    - Jeg vidste allerede meget af dette, men det er godt at få en opfrisker på OSI og TCP/IP 
    
=== "Jesper"
        
    - Vi har arbejdet med CIS kontroller og CIS benchmark
        - Jeg synes I begyndelsen at det virkede lidt overskueligt hvordan man vurdere en virksomheds implementaion gruppe, men jeg blev heldigvis klogere I slutnignen af faget,
        - Det var lidt syndt at det var Online undervisingen, det er ikke lige mig. Der var desværre ikke så meget man kan gøre ved det.
        - Jeg synes opgaverne var rigtige gode, da det gav en indsigt I hvordan det bliver brugt I praksis.
    
=== "John"

    - apparmor var spændende at installerer men der manglede lidt en demonstration for hvad det gjorde helt præcist. Jeg så og forstå den satte profiler i en position. og der var motd osv til at villede uatoriseret brugere. Men udover det forstod jeg det nok ikke helt. 


## Andet

**I denne opgave skal gruppen lave vurderinger af en fiktiv virksomhed og dens IT-sikkerhedsmodenhedsniveau i henhold til risikoprofil og størrelse.**

_BS Consulting A/S er en mellemstor virksomhed, der opererer inden for teknologibranchen. De leverer softwareløsninger til kunder over hele verden og opbevarer følsomme kundedata på deres systemer. Virksomheden har været bekymret for deres cybersikkerhedspraksis og ønsker at forbedre deres modenhed på dette område. De har bedt om hjælp til at vurdere deres nuværende cybersikkerhedssituation og identificere, hvilken implementeringsgruppe (IG) de mest sandsynligt hører til._

**Nuværende Situation:**



1. BS Consulting A/S har etableret en IT-afdeling, der er ansvarlig for at håndtere deres cybersikkerhedsbehov.
2. De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer.
3. Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder.
4. Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
5. Virksomheden har en reaktiv tilgang til cybersikkerhed, hvor de kun reagerer på hændelser, når de opstår, i stedet for at have proaktive sikkerhedsforanstaltninger på plads.


## Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/47_CIS_Kontroller/#instruktioner)

Vurder BS Consulting A/S' nuværende cybersikkerhedspraksis og identificer, hvilken implementeringsgruppe (IG) BS Consulting A/S sandsynligvis hører til baseret på deres nuværende cybersikkerhedspraksis. Beskriv jeres begrundelse for vurderingen.


## Forslag til spørgsmål som gruppen kan bruge til sin vurdering



1. **Hvordan vurderer du BS Consulting A/S' nuværende cybersikkerhedspraksis i forhold til de forskellige CIS 18 kontroller?**
* Vi mener De kun overholder 2 kontroller max og ligger i gruppe 1. Der er meget de kan forbedre sig:
    * mangler cis1-5 og alt mulig andet
2. **Hvilken implementeringsgruppe (IG) mener du, BS Consulting A/S hører til, og hvorfor?**

Gruppe 1. De overholder:



* antivirus  **CIS12**
* adgangskontrol
* <span style="text-decoration:underline;">Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.</span>
3. **Hvad kunne BS Consulting A/S gøre for at forbedre deres cybersikkerhedsmodenhed i forhold til deres risikoprofil og størrelse** **samt bevæge sig til en højere implementeringsgruppe?**
* De kan overholde CIS14 ved at træne personalet i deres sikkerhedspolitik
* Virksomheden skal ændres til en proaktiv tilgang til cybersikkerhed, hvor de forebygger hændelser, inden de opstår, og det mindsker angrebsfladen.




## Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/49_Mitre_ATT_CK/#information)

Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.

Opgaven er en eftermiddagsopgave, og det forventes, at gruppen laver undersøgelsen sammen.

**I bliver kastet ud i den dybe ende med denne opgave. Det er meningen med øvelsen (Det og naturligvis at I skal lære Mitre at kende)**


## Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/49_Mitre_ATT_CK/#instruktioner)



1. **Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik?**
    1. **Privilege Escalation**
        1. The adversary is trying to gain higher-level permissions. Privilege Escalation consists of techniques that adversaries use to gain higher-level permissions on a system or network. 
        2. Adversaries can often enter and explore a network with unprivileged access but require elevated permissions to follow through on their objectives. Common approaches are to take advantage of system weaknesses, misconfigurations. - MITRE ATTACK
2. **Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation.**
    2.  Abuse Elevation Control Mechanism

    T1548.001: Abuse Elevation Control Mechanism:


        Exploits system mechanisms for higher privileges.


        Examples: Misusing UAC in Windows, exploiting sudo in Unix/Linux.


    T1548.002: Bypass User Account Control (UAC):


        Circumvents Windows UAC, which requires admin approval for system changes.


    T1548.003: Hooking:


        Alters system behavior by replacing functions.


        Enables manipulation of access controls for privilege escalation.


    T1548.004: Process Injection:


        Inserts malicious code into running processes.


        Grants attacker the same privileges as the target process.


    T1548.005: Exploitation for Privilege Escalation:


        Exploits known vulnerabilities for higher privileges.


        Targets OS, software, or services vulnerabilities for elevated control.

3. **Identificer og undersøg Mitigeringen M1026.**
    3. **Privileged Account Management**

Mitigation M1026 fokuserer på at reducere risikoen for privilegieeskaleration ved at implementere sikkerhedsforanstaltninger og bedste praksis. Her er en undersøgelse af denne mitigation:

anvend principle of least privilege



4. **Identificer og undersøg detekteringen DS0022.**
* Overvågning af ændringer i UAC-indstillinger og -konfigurationer.
* Analyse af begivenhedslogs for ændringer relateret til UAC-deaktivering eller omgåelse.
* Overvågning af systemændringer, der potentielt tillader omgåelse af UAC, såsom ændringer i politikker, registreringsdatabasen eller systemkonfigurationsfiler.
* Analyse af adgangsforsøg til UAC-relaterede systemkomponenter og -filer.





5. **Til næste uge, forbered en kort præsentation, der opsummerer dine resultater, herunder en forklaring på Privilege Escalation-taktikken, teknikkerne under T1548, mitigeringen M1026 samt detekteringen DS0022.**

_Det behøver ikke at være en stor og forkromet præsentation, blot noget i stil med "Daniel, gæstelæreren", når han præsenterer resultatet af sine øvelser. Altså maks 5 minutter_



1. H**vad er formålet med Privilege Escalation-taktikken (TA0004) inden for Mitre ATT&CK-rammeværket, og hvorfor er det en vigtig del af en angribers taktikker?**
    1. The adversary is trying to gain higher-level permissions. Privilege Escalation consists of techniques that adversaries use to gain higher-level permissions on a system or network. Adversaries can often enter and explore a network with unprivileged access but require elevated permissions to follow through on their objectives. Common approaches are to take advantage of system weaknesses, misconfigurations - MITRE ATT&CK
2. **Hvordan bidrager teknikkerne under T1548 til angriberens evne til at opnå Privilege Escalation?**
    2. Adversaries may circumvent mechanisms designed to control elevate privileges to gain higher-level permissions. Most modern systems contain native elevation control mechanisms that are intended to limit privileges that a user can perform on a machine. Authorization has to be granted to specific users in order to perform tasks that can be considered of higher risk. An adversary can perform several methods - MITRE ATT&CK





3. **Hvad er karakteristika for mitigeringen M1026, og hvordan hjælper den med at afhjælpe risiciene forbundet med Privilege Escalation?**
    3. Manage the creation, modification, use, and permissions associated to privileged accounts, including SYSTEM and root. - MITRE ATT&CK