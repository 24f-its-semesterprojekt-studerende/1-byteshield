---
 hide:
#  - footer
---

# Læringslog uge 07 - *Kommunikation -og netværkssikkerhed*

## Emner

- OSI modellen
- Basale kommandoer i Nmap
- Design et Netværksdiagram
- Sniffe traffik med Wireshark

## Mål for ugen

At kunne navigere rundt i Linux med kommando linje (bash)

## Praktiske mål

## Praktiske opgaver vi har udført
**Vi har i løbet af dagen arbejdet med 6 opgaver:**


### 1. OSI modellen
??? Info "Åbn mig for at se opgave svar"
    ![32bits](images/Network/OSI%20Model.PNG)


### 2. OSI modellen og protokoller
??? Info "Åbn mig for at se opgave svar"

    ![OSI](images/Network/OSI%20model%20Protocol.PNG)

    HTTP er usikkert da data sendes i plain text, ergo hvis en hjemmeside kun gør brug af HTTP og hjemmesiden har en loginside kan en trusselaktør se password og brugernavn ved at sniffe netværket eller udfører et Man In The Middel attack MiTM, når en person logger ind.

    - QUIC IETF erstatter TCP med udp med Handshake kvaliteter

    - Kildehenvisning
        - [What is HTTP Vulnerability & Its Types (purevpn.com)](https://www.purevpn.com/ddos/http-vulnerability)


### 3. Wireshark og OSI modellen
??? Info "Åbn mig for at se opgave svar"

    ![Wireshark](images/Network/wireshark.PNG)

    - TCP illustreret

    ![RFC](images/Network/RFC.PNG)

    [https://www.rfc-editor.org/rfc/rfc9293.pdf](https://www.rfc-editor.org/rfc/rfc9293.pdf) side 7 & 8 


    ![Wireshark](images/Network/wireshark3.PNG)

    - Hvor mange cipher suites understøtter klienten?
        - Der 17 Cipher suites
    - Hvilke TLS versioner understøtter klienten?
        - TLS 1.2

    ![Wireshark](images/Network/wireshark4.PNG)

    - Hvilken cipher suite vælger serveren?
        - TLS_AES_128_GCM_SHA256
    - Hvilken TLS version vælger serveren?
        - TLS 1.2


### 4. OPNsense udforskning, hvad er OPNsense og hvad kan det?

??? Info "Åbn mig for at se opgave svar"

    1. Hvilke features tilbyder opnsense?
        - OPNsense Core Features
            - Traffic Shaper
            - Captive portal
            - Voucher support
            - Template manager
            - Multi zone support
            - Forward Caching Proxy
            - Transparent mode supported
            - Blacklist support
            - Virtual Private Network
            - Site to site
            - Road warrior
            - IPsec
            - OpenVPN
            - High Availability & Hardware Failover
            - Includes configuration synchronization & synchronized state tables
            - Moving virtual IPs
            - Intrusion Detection and Inline Prevention
            - Built-in support for Emerging Threats rules
            - Simple setup by use of rule categories
            - Scheduler for period automatic updates
            - Built-in reporting and monitoring tools
            - System Health, the modern take on RRD Graphs
            - Packet Capture
            - Netflow
            - Support for plugins
            - DNS Server & DNS Forwarder
            - DHCP Server and Relay
            - Dynamic DNS
            - Backup & Restore
            - Encrypted cloud backup to Google Drive and Nextcloud
            - Configuration history with colored diff support
            - Local drive backup & restore
            - Stateful inspection firewall
            - Granular control over state table
            - 802.1Q VLAN support
    
    2. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder?
        - OPNsense kommer med en integreret sikkerhedskontrol for kendte sårbarheder, som kan findes i deres firmwaremodul. I dette tilfælde har man muligheden for selv at verificere, hvad risikoen er ved at fortsætte med at bruge den nuværende version lidt længere.
 
        - Du kan se siden ved at gå til: System -> Firmware her vil en knap vise sig som hedder: “Run an Audit” hvis du trykker på knappen vil en sikkerhedsrapport blive vist.
    
        - [Security Reporting - OPNsense documentation](https://docs.opnsense.org/security.html#reporting-an-incident)


        - ![Opnsense](images/Network/opnsense1.PNG)

        - ![Opnsense](images/Network/opnsense2.PNG)


    3. Hvad er lobby og hvad kan man gøre derfra?
        - Lobby er indgangen til ens virtuelle sikkerheds enhed
        - Her kan man finde sit dashboard, ændrer password og afslutte en session

    4. Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det?
        - Ja, man kan se det under "reporting" sektionen. Her kan du se information om trafikstrømme I ens netværk
    
        - ![Opensense](images/Network/opnsense3.PNG)

   
    5. Hvad kan man se omkring trafik i Reporting?
        - Her kan man detaljeret information omkring trafik, herunder statistikker, grafer, og rapporter om forskellige typer af trafik, såsom indgående og udgåednde trafik, applikationsniveau traffik.

        - ![Opensense](images/Network/opnsense4.PNG)

    6. Hvor oprettes vlan's og hvilken IEEE standard anvendes?
        - Under interfaces. IEEE standarder:
            ○ 802.1Q


    7. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?
        - Det ser ud til der er konfigureret firewall regler for både LAN og WAN interfacet

        - ![Opensense](images/Network/opnsensefirewallruleLAN.PNG)

        - ![Opensense](images/Network/opnsensefirewallruleWAN.PNG)

    8. Hvilke slags VPN understøtter opnsense?
        - Openvpn
        - Ipsec
        - Wireguard

    9. Hvilket IDS indeholder opnsense?
        - Ja, baseret på Suricata 
        - Den kan både fungere som IDS og IPS

        - [Intrusion Prevention System — OPNsense documentation](https://docs.opnsense.org/manual/ips.html)
    
    10. Hvor kan man installere os-theme-cicada i opnsense?
        - Gå til System > Firmware > Plugins her kan du finde de themes du kan installere
        - Gå til System> Settings > General for at sætte dit nye theme


    11. Hvordan opdaterer man firmware på opnsense?
        - Opdatering kan blive installeret fra web interfacet ved at gå til: System ‣ Firmware ‣ Status. På denne side kan du klikke på "check for updates" hvis der en ny ledig vil den blive installeret
    
        - [Update Manual - OPNsense documentation](https://docs.opnsense.org/manual/updates.html)
    
        - ![Opensense](images/Network/opnsense7.PNG)


    12. Hvad er nyeste version af opnsense?
        - 24.1 "Savvy Shark" Series?

    13. Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)
        - Nej, men er opdateret efterfølgende.



### 5. Nmap THM rum

??? Info "Åbn mig for at se opgave svar"

    Vi lærer om brugen af Nmap. 

    - ![nmap](images/Network/nmap1.PNG)

    - -sT (TCP Connect Scan): This option in Nmap performs a TCP connect scan, where Nmap attempts to establish a full TCP connection with the target ports to determine whether they are open, closed, or filtered by firewalls.

    - -f (Fast Mode): This option instructs Nmap to scan more aggressively by skipping some of the host discovery phases, potentially speeding up the scan process. It's useful when you want results quickly and are less concerned about being stealthy.

    - -r (Reverse Order): With this option, Nmap scans ports in reverse order, starting from the highest port number and moving downwards. This might be helpful in certain scenarios where you want to prioritize scanning higher ports first.

    ![nmap2](images/Network/nmap2.PNG)

    - Her kan vi se hvilket porte der er åbne.

    ![nmap3](images/Network/nmap3.PNG)

    ![nmap4](images/Network/nmap4.PNG)

    ![nmap5](images/Network/nmap5.PNG)

    - Resten af billederne her er gode kommandoer at huske, samt deres beskrivelse.



### 6. Netværksdiagram
??? Info "Åbn mig for at se opgave svar"
    ![Networkdiagram](images/Network/Networkdiagram.PNG)

    - ps. Christian siger det korrekt 😅

## Læringsmål

**Læringsmål vi har arbejdet med**

- Den studerende har viden om og forståelse for

- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Adressering i de forskellige lag
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, - - IDS/IPS, honeypot, DPI).
- Netværkstrusler

- Den studerende kan
    - Identificere sårbarheder som et netværk kan have
    - Den studerende kan håndtere udviklingsorienterede situationer herunder
    - Monitorere og administrere et netværks komponenter

- Læringsmål den studerende kan bruge til selvvurdering
    - Den studerende kan forklare OSI modellen og de tilhørende lag
    - Den studerende kan placere hardware på de forskellige lag i OSI modellen
    - Den studerende kan forklare hvad en protokol er og hvordan den er opbygget




## Reflektioner over hvad vi har lært

=== "Christian"

    - Kunne meget godt lide at arbejde med opsætningen, da det godt kan blive tunget med oplæg osv.

=== "Jesper"
        
    - Vi lavede en masse opgave omkring nogle lidt forskellige ting. Jeg tror jeg er lidt forvirret omkring hvad det vi kommer til at skulle bruge det til i det store hele billede.
    - Jeg kunne godt lide den måde vi arbejdede på med opgaverne, dvs. med andre og at det var meget praktisk.
    - Jeg skal dog blive bedre til at skrive refleksioner ned efter undervisningen ellers glemmer jeg det, som man kan fornæmme ud for denne refleksion.
    
=== "John"

    - Meget info at tage ind. Men det ringer nogle genkendte klokker. OPNsense kan en masse fremmed jeg ikke ved om jeg skal lærer eller bruge.

    


## Andet