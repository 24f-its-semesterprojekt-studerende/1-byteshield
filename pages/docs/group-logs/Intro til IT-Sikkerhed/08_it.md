---
 hide:
#  - footer
---

# Læringslog uge 08 - *Intro til IT-sikkerhed*

## Emner

- Ugens emner er:
    - Sikkerhed i netværksprotokoller
    - Wireshark
    - Opsætning af virtuelle maskiner
    - Tryhackme øvelser
    - Common Vulnerabilities and Exposures
    - OSINT



## Udførelse af Opgave

### Wireshark

![hierarki](images/IntroSecurity/hierarki.png)

- Vi har lært at navigerer i wireshark ud fra tryhackme øvelser. 
- Her er et billede der viser hierarki. Godt til at læse OSI lag

### Opsætning af Netværk

![Opsætning](images/IntroSecurity/opsætning.png)

- Vi har opsat flere virtuelle maskiner og et netværk til dem. Ud fra billedet.

![1](images/IntroSecurity/1.png)

- Konfigurerering / installationsproces

![2](images/IntroSecurity/2.png)

- Angiver det rigtige VMNet

![3](images/IntroSecurity/3.png)

- Opsætning af Router

#### Anvender Putty

![4](images/IntroSecurity/4.png)

- Giver Host-Name

![5](images/IntroSecurity/5.png)

- linker gateway addresser

![6](images/IntroSecurity/6.png)

- Indstiller security zones

![7](images/IntroSecurity/7.png)

- Overblik 

![8](images/IntroSecurity/8.png)

- Route Terse

![9](images/IntroSecurity/9.png)

- Security zones Overblik




## Notater

- Common Vulnerabilities and Exposures (CVE)
    - Det er en database der indeholder kendte og hyppige anvendte sårbarheder.
        - Hvert indslag har et  id nummer
    - Meltdown
        - bryder isolation imellem operativsystem og brugerapplikationer
    - Spectre
        - bryder forskellige applikationer og skaffer error free programmers hemmeligheder
    - LOG4J
        - Kendt framework som pludseligt blev sårbart
            - tillader RCE (Remode Code Execution)

- OSINT (Open Source Intelligence)
     - godt for trusselaktør
        - laver ikke støj når de undersøgern nogle.

    - Værktøjer
        - Webscraper 
        - Osinter



## Reflektioner over hvad vi har lært
=== "Jesper"

=== "John"
    - Putty er nice fordi man kan copy paste ting ind i den vm
    - Quic IETF forklaret som UDP med TCP kvaliteter virkede ret sejt
